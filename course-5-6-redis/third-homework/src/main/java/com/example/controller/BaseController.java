package com.example.controller;

import com.example.service.PostService;
import com.example.utils.RedisUtil;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public class BaseController {

    @Autowired
    HttpServletRequest req;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    PostService postService;

    @Autowired
    RedissonClient redissonClient;
}
