package com.example.config;

import com.example.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Order(1000)
@Component
public class ContextStartup implements ApplicationRunner {

    @Autowired
    PostService postService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        postService.initIndexWeekRank();
    }
}
