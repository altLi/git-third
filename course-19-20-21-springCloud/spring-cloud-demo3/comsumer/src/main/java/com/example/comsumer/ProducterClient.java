package com.example.comsumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Component
//@FeignClient(name = "PRODUCTER-CLIENT", fallback=ProducterClientFallback.class)
@FeignClient(name = "PRODUCTER-CLIENT", fallbackFactory = ProducterClientFallbackFactory.class)
public interface ProducterClient {

    @GetMapping("/order/{id}")
    String findById(@PathVariable("id") Long id);
}
